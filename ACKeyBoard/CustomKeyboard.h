//
//  CustomKeyboard.h
//  CustomKeyboardSample
//
//  Created by JinLong on 12/5/14.
//  Copyright (c) 2014 Steven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomKeyboard : UIView
@property (weak, nonatomic) IBOutlet UIButton *buttonA;

@property (weak, nonatomic) IBOutlet UIButton *buttonB;
@property (weak, nonatomic) IBOutlet UIButton *buttonSpace;

@property (weak, nonatomic) IBOutlet UIButton *buttonReturn;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;
@property (weak, nonatomic) IBOutlet UIButton *buttonImageFavorite;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage60;

@property (weak, nonatomic) IBOutlet UIButton *buttonNextKeyboard;

@end

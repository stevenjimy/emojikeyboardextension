//
//  GrayPageControl.h
//
//  Created by blue on 12-9-28.
//
//  Created by Steven on 14-11-15.
//  Copyright (c) 2014年 Steven Jimy. All rights reserved.
//
//

#import <UIKit/UIKit.h>


@interface GrayPageControl : UIPageControl {

    UIImage* activeImage;

    UIImage* inactiveImage;
}


@end
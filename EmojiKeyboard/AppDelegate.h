//
//  AppDelegate.h
//  EmojiKeyboard
//
//  Created by JinLong on 12/8/14.
//  Copyright (c) 2014 Steven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

